//
//  ViewController.swift
//  VXTranslateDemo
//
//  Created by zhangxin on 2023/7/25.
//  tips：翻译需要输入准确的语意，比如：”一条美丽小河“会翻译出错crash，“一条河”则能翻译正确

import UIKit

class ViewController: UIViewController {
    private var translator: Translator!
    override func viewDidLoad() {
        super.viewDidLoad()
        let options = TranslatorOptions(sourceLanguage: TranslateLanguage.chinese, targetLanguage: TranslateLanguage.english)
        let translator = Translator.translator(options: options)
        self.translator = translator
        /*
         模型不一定需要下载，可以直接调用translate
        self.translator.downloadModelIfNeeded {[weak self] error in
            guard error == nil else {
                return
            }
            print("download success")
            //tips：翻译的文案不能随便写，一定要正确的英文，不然会crash，无语
            let text = "china"
            self?.translator.translate(text) { result, error in
                guard error == nil else {
                    return
                }
                print("翻译的结果"+(result ?? ""))
            }
        }
         */
        //tips：翻译的文案不能随便写，一定要正确的英文，不然会crash，无语
        let text = "一条河"
        self.translator.translate(text) { result, error in
            guard error == nil else {
                return
            }
            print("翻译的结果："+(result ?? ""))
        }
    }


}

